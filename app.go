package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"text/template"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

type User struct {
	Object string  `json:"object,omitempty"`
	Entry  []Entry `json:"entry,omitempty"`
}

type Entry struct {
	ID     string   `json:"id,omitempty"`
	UID    string   `json:"uid,omitempty"`
	Time   int      `json:"time,omitempty"`
	Change []Change `json:"changes,omitempty"`
}

type Change struct {
	Field string `json:"field,omitempty"`
	Value string `json:"value,omitempty"`
}

func (a *App) Initialize(dbuser, dbpass, dbhost, dbport, dbname string) {
	connectionstring := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbuser, dbpass, dbhost, dbport, dbname)
	var err error
	a.DB, err = sql.Open("mysql", connectionstring)
	if err != nil {
		log.Fatal(err)
	}
	a.Router = mux.NewRouter()
	a.initRoutes()
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (a *App) initRoutes() {
	// Webhooks APIs
	a.Router.HandleFunc("/fbwebhooks", a.VerifyEndPoint).Methods("GET")
	a.Router.HandleFunc("/fbwebhooks", a.MessageEndPoint).Methods("POST")
	// App APIs
	a.Router.HandleFunc("/", a.Home).Methods("GET")
	a.Router.HandleFunc("/users", a.getUsers).Methods("GET")
	a.Router.HandleFunc("/users/search/{keyword}", a.getUsersSearch).Methods("GET")
	a.Router.HandleFunc("/user/{id}", a.getUser).Methods("GET")
	a.Router.HandleFunc("/user/{id}/changes", a.getUserChanges).Methods("GET")
	a.Router.HandleFunc("/user/{id}/changes/search/{keyword}", a.getUserSearchChanges).Methods("GET")
}

func (a *App) Home(w http.ResponseWriter, r *http.Request) {
	tpl := template.Must(template.ParseFiles("template/index.html"))
	tpl.Execute(w, nil)
}

func (a *App) VerifyEndPoint(w http.ResponseWriter, r *http.Request) {
	mode := r.URL.Query().Get("hub.mode")
	challenge := r.URL.Query().Get("hub.challenge")
	token := r.URL.Query().Get("hub.verify_token")

	if mode != "" && token == VERIFYTOKEN {
		w.WriteHeader(200)
		w.Write([]byte(challenge))
	} else {
		w.WriteHeader(404)
		w.Write([]byte("Error, wrong VERIFY_TOKEN"))
	}
}

func (a *App) MessageEndPoint(w http.ResponseWriter, r *http.Request) {
	readJSON, _ := ioutil.ReadAll(r.Body)
	var user User
	json.Unmarshal(readJSON, &user)
	for i := 0; i < len(user.Entry); i++ {
		fid := user.Entry[i].ID
		fields := user.Entry[i].Change[0].Field
		values := user.Entry[i].Change[0].Value
		times := user.Entry[i].Time

		var matched int
		err := a.DB.QueryRow("SELECT f_id FROM users WHERE (f_id=?)", fid).Scan(&matched)
		if err != nil {
			stmt, err := a.DB.Prepare("INSERT INTO users SET f_id=?, " + fields + "=?, created_at=NOW(), updated_at=NOW()")
			if err != nil {
				return
			}
			res, err := stmt.Exec(fid, values)
			if err != nil {
				return
			}
			res.LastInsertId()
		} else {
			stmt, err := a.DB.Prepare("UPDATE users SET " + fields + "=?, updated_at=NOW() WHERE (f_id=?)")
			if err != nil {
				return
			}
			res, err := stmt.Exec(values, fid)
			if err != nil {
				return
			}
			res.RowsAffected()
		}

		stmt, err := a.DB.Prepare("INSERT INTO changes SET f_id=?, field=?, value=?, time=?, created_at=NOW()")
		if err != nil {
			return
		}
		res, err := stmt.Exec(fid, fields, values, times)
		if err != nil {
			return
		}
		res.LastInsertId()
	}
	w.WriteHeader(http.StatusOK)
}

func (a *App) getUsers(w http.ResponseWriter, r *http.Request) {
	users, err := getUsers(a.DB)
	if err != nil {
		responseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, users)
}

func (a *App) getUsersSearch(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	keyword := vars["keyword"]
	users, err := getUsersSearch(a.DB, keyword)
	if err != nil {
		responseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, users)
}

func (a *App) getUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fid := vars["id"]
	u := Users{FID: fid}
	if err := u.getUser(a.DB); err != nil {
		switch err {
		case sql.ErrNoRows:
			responseWithError(w, http.StatusNotFound, "Not found any user")
		default:
			responseWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}
	responseWithJSON(w, http.StatusOK, u)
}

func (a *App) getUserChanges(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fid := vars["id"]
	changes, err := getUserChanges(a.DB, fid)
	if err != nil {
		responseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, changes)
}

func (a *App) getUserSearchChanges(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fid := vars["id"]
	keyword := vars["keyword"]
	changes, err := getUserSearchChanges(a.DB, fid, keyword)
	if err != nil {
		responseWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, changes)
}

func responseWithError(w http.ResponseWriter, code int, message string) {
	responseWithJSON(w, code, map[string]string{"error": message})
}

func responseWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
