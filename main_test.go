package main

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

const (
	tableChangesCreation = `
		CREATE TABLE IF NOT EXISTS changes (
			id int(11) NOT NULL AUTO_INCREMENT,
			f_id varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
			field varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			value text COLLATE utf8mb4_unicode_ci,
			time int(11) DEFAULT NULL,
			created_at timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			FULLTEXT KEY changes (field,value)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
	`
	tableUsersCreation = `
		CREATE TABLE IF NOT EXISTS users (
			id int(10) unsigned NOT NULL AUTO_INCREMENT,
			f_id varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
			name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			first_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			middle_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			last_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			email varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			birthday varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			gender varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
			about text COLLATE utf8mb4_unicode_ci,
			education text COLLATE utf8mb4_unicode_ci,
			location text COLLATE utf8mb4_unicode_ci,
			locale text COLLATE utf8mb4_unicode_ci,
			created_at timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
			updated_at timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (id),
			FULLTEXT KEY users (f_id,name,first_name,middle_name,last_name,email,gender,location)
		) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
	`
)

var a App

func TestMain(m *testing.M) {
	a = App{}
	a.Initialize("root", "", "localhost", "3306", "fbtracker")
	checkRequirementTablesExists()
	code := m.Run()
	clearChangesTable()
	clearUsersTable()
	os.Exit(code)
}

func TestMessageEndPoint(t *testing.T) {
	clearChangesTable()
	clearUsersTable()
	// payload from Fackbook Graph API
	json := `
		{
			"entry": [
					{
							"time": 1515594815,
							"changes": [
									{
											"field": "first_name",
											"value": "Krit"
									}
							],
							"id": "123456789",
							"uid": "123456789"
					}
			],
			"object": "user"
		}
	`
	payload := []byte(json)
	req, _ := http.NewRequest("POST", "/fbwebhooks", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestGetUsers(t *testing.T) {
	req, _ := http.NewRequest("GET", "/users", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected data array. Got %s", body)
	}
}

func TestGetUsersSearch(t *testing.T) {
	req, _ := http.NewRequest("GET", "/users/search/123456789", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected user. Got %s", body)
	}
}

func TestGetUser(t *testing.T) {
	//clearUsersTable()
	req, _ := http.NewRequest("GET", "/user/123456789", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected user. Got %s", body)
	}
}

func TestGetUserChanges(t *testing.T) {
	//clearChangesTable()
	req, _ := http.NewRequest("GET", "/user/123456789/changes", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected user's changes. Got %s", body)
	}
}

func TestGetUserSearchChanges(t *testing.T) {
	//clearChangesTable()
	req, _ := http.NewRequest("GET", "/user/123456789/changes/search/first", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	if body := response.Body.String(); body == "[]" {
		t.Errorf("Expected user's changes. Got %s", body)
	}
}

func checkRequirementTablesExists() {
	if _, err := a.DB.Exec(tableChangesCreation); err != nil {
		log.Fatal(err)
	}
	if _, err := a.DB.Exec(tableUsersCreation); err != nil {
		log.Fatal(err)
	}
}

func clearChangesTable() {
	a.DB.Exec("DELETE FROM changes")
	a.DB.Exec("ALTER TABLE changes AUTO_INCREMENT=1")
}

func clearUsersTable() {
	a.DB.Exec("DELETE FROM users")
	a.DB.Exec("ALTER TABLE users AUTO_INCREMENT=1")
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)
	return rr
}
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
