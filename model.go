package main

import (
	"database/sql"
	"fmt"
)

type Users struct {
	FID        string  `json:"f_id,omitempty"`
	Name       *string `json:"name"`
	Firstname  *string `json:"first_name"`
	Middlename *string `json:"middle_name"`
	Lastname   *string `json:"last_name"`
	Email      *string `json:"email"`
	Birthday   *string `json:"birthday"`
	Gender     *string `json:"gender"`
	Location   *string `json:"location"`
	Created    *string `json:"created_at"`
	Updated    *string `json:"updated_at"`
}

type Changes struct {
	FID     string  `json:"f_id,omitempty"`
	Field   *string `json:"field"`
	Value   *string `json:"value"`
	Time    *int    `json:"time"`
	Created *string `json:"created_at"`
}

func getUsers(db *sql.DB) ([]Users, error) {
	sql := `
		SELECT f_id, name, first_name, middle_name, last_name, email, birthday, gender, location, created_at, updated_at
		FROM users
		ORDER BY f_id ASC
	`
	rows, err := db.Query(sql)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []Users{}
	for rows.Next() {
		var u Users
		if err := rows.Scan(
			&u.FID,
			&u.Name,
			&u.Firstname,
			&u.Middlename,
			&u.Lastname,
			&u.Email,
			&u.Birthday,
			&u.Gender,
			&u.Location,
			&u.Created,
			&u.Updated,
		); err != nil {
			return nil, err
		}
		users = append(users, u)
	}
	return users, nil
}

func getUsersSearch(db *sql.DB, keyword string) ([]Users, error) {
	sql := `
		SELECT f_id, name, first_name, middle_name, last_name, email, birthday, gender, location, created_at, updated_at
		FROM users
		WHERE MATCH(f_id, name, first_name, middle_name, last_name, email, gender, location)
		AGAINST('*%s*' IN BOOLEAN MODE)
		ORDER BY f_id ASC
	`
	stmt := fmt.Sprintf(sql, keyword)
	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []Users{}
	for rows.Next() {
		var u Users
		if err := rows.Scan(
			&u.FID,
			&u.Name,
			&u.Firstname,
			&u.Middlename,
			&u.Lastname,
			&u.Email,
			&u.Birthday,
			&u.Gender,
			&u.Location,
			&u.Created,
			&u.Updated,
		); err != nil {
			return nil, err
		}
		users = append(users, u)
	}
	return users, nil
}

func (u *Users) getUser(db *sql.DB) error {
	sql := `
		SELECT f_id, name, first_name, middle_name, last_name, email, birthday, gender, location, created_at, updated_at
		FROM users
		WHERE f_id='%s'
	`
	stmt := fmt.Sprintf(sql, u.FID)
	return db.QueryRow(stmt).Scan(
		&u.FID,
		&u.Name,
		&u.Firstname,
		&u.Middlename,
		&u.Lastname,
		&u.Email,
		&u.Birthday,
		&u.Gender,
		&u.Location,
		&u.Created,
		&u.Updated,
	)
}

func getUserChanges(db *sql.DB, fid string) ([]Changes, error) {
	sql := `
		SELECT f_id, field, value, time, created_at
		FROM changes
		WHERE f_id='%s'
		ORDER BY created_at DESC
	`
	stmt := fmt.Sprintf(sql, fid)
	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	changes := []Changes{}
	for rows.Next() {
		var c Changes
		if err := rows.Scan(
			&c.FID,
			&c.Field,
			&c.Value,
			&c.Time,
			&c.Created,
		); err != nil {
			return nil, err
		}
		changes = append(changes, c)
	}
	return changes, nil
}

func getUserSearchChanges(db *sql.DB, fid, keyword string) ([]Changes, error) {
	sql := `
		SELECT f_id, field, value, time, created_at
		FROM changes
		WHERE (f_id='%s') AND MATCH(field, value)
		AGAINST('*%s*' IN BOOLEAN MODE)
		ORDER BY created_at DESC
	`
	stmt := fmt.Sprintf(sql, fid, keyword)
	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	changes := []Changes{}
	for rows.Next() {
		var c Changes
		if err := rows.Scan(
			&c.FID,
			&c.Field,
			&c.Value,
			&c.Time,
			&c.Created,
		); err != nil {
			return nil, err
		}
		changes = append(changes, c)
	}
	return changes, nil
}
